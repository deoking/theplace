//Theme = Doge
// Objective = Maze with Challenges, Bullets
// Creater = Fiach McHugh

var gCon = function()
{
	//Canvas
	this.canvas = document.getElementById('game-canvas'),
	this.context = this.canvas.getContext('2d'),
	this.gameStarted = false,


	//Counters
	this.fpsElement = document.getElementById('fps'),
	this.scoreElement = document.getElementById('score'),

	//Time
	this.lastAnimationFrameTime = 0,
	this.fps = 60,
	this.lastFpsUpdateTime = 0,

	//Sprites

	//Pause
	this.paused = false,
	this.PAUSED_CHECK_INTERVAL = 175,
	this.pauseStartTime,
	this.windowHasFocus = true,
	this.countdownInProgress = false,
	this.SHORT_DELAY = 50,

	//Loading
	this.loadingElement = document.getElementById('loading'),
	this.loadingTitleElement = document.getElementById('loading-title'),
	this.AnimatedGifElement = document.getElementById('loading-animated-gif'),

	//TOAST
	this.toastElement = document.getElementById('toast')
	this.TRANSPARENT = 0,
	this.OPAQUE = 1.0,

	//Instructions
	this.instructionsElement = document.getElementById('instructions'),
	
	//copyright
	this.copyRightElement = document.getElementById('copyright'),

	// Game objects
	this.player = {
		speed: 300, // movement in pixels per second
		x: this.canvas.width / 2,
		y: this.canvas.height / 2
	},
	this.enemy = {
		x: 0,
		y: 0
	},

	//Socket stuff
   	this.serverAvailable=true;
   	try{
      this.socket = new io.connect('http://localhost:49800');
   	}
   	catch(err)
   	{
      this.serverAvailable = false;
   	}
   	console.log("Successfully connected to the server!");

	//temp Images
	this.background = new Image(),
	this.playerImg = new Image(),
	this.enemyImg = new Image();
}

// Laucher
gCon.prototype = 
{
	//createSprites
	//PostionSprites
	//InitializeSprites

	//IntitalizeImages
	initializeImages: function()
	{
		this.background.src = 'images/background.jpg';
		this.playerImg.src = 'images/player.gif';
		this.enemyImg.src = 'images/doge.jpg';

		this.background.onload = function(e)
		{
			gCon.backgroundLoaded();
			gCon.startGame();
		};
	},
	//StartGame
	startGame: function()
	{
		this.revealGame();
		this.revealInitialToast();
		window.requestAnimationFrame(this.animate);
		this.reset();

	},

	backgroundLoaded: function()
	{
		var LOADIND_SCREEN_TRANSITION_DURATION = 1500;
		this.fadeOutElements(this.loadingElement, LOADIND_SCREEN_TRANSITION_DURATION);

		setTimeout( function(){
			gCon.startGame();
			gCon.gameStarted = true;
		}, LOADIND_SCREEN_TRANSITION_DURATION);
	},

	

	//animate
	animate: function(now)
	{
		if(gCon.paused)
		{
			setTimeout(function ()
			{
				requestAnimationFrame(gCon.animate);
			}, gCon.PAUSED_CHECK_INTERVAL);
		}
		else
		{
			gCon.fps = gCon.calculateFps(now);
			gCon.draw(now);
			gCon.playerUpdate(now);
			gCon.lastAnimationFrameTime = now;
			requestAnimationFrame(gCon.animate);
			gCon.checkCollision();
			
		}
		
	},
	//draw
	draw: function(now)
	{
		this.drawBackground();
		this.drawPlayer();
		this.drawEnemy();
	},
	//player Movement dectection, When pause keys aren't active
	playerUpdate: function(now)
	{
		if (38 in keysDown || 87 in keysDown) 
		{ // Player holding up
			this.player.y -= this.player.speed * ((now-this.lastAnimationFrameTime)/1000);
		}
		if (40 in keysDown || 83 in keysDown) 
		{ // Player holding down
			this.player.y += this.player.speed * ((now-this.lastAnimationFrameTime)/1000);
		}
		if (37 in keysDown || 65 in keysDown) 
		{ // Player holding left
			this.player.x -= this.player.speed * ((now-this.lastAnimationFrameTime)/1000);
		}
		if (39 in keysDown || 68 in keysDown) 
		{ // Player holding right
			this.player.x += this.player.speed * ((now-this.lastAnimationFrameTime)/1000);
		}
	},

	//calculateFps
	calculateFps: function(now)
	{
			var fps =1/(now-this.lastAnimationFrameTime) * 1000;
			if(now -this.lastFpsUpdateTime > 1000)
			{
				this.lastFpsUpdateTime = now;
				this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
			}
			return fps;
	},

	checkCollision: function()
	{
		//Collision with doge
		if (
		this.player.x <= (this.enemy.x + 32) //32 pixels Size of Doge collision area
		&& this.enemy.x <= (this.player.x + 32)
		&& this.player.y <= (this.enemy.y + 32)
		&& this.enemy.y <= (this.player.y + 32)
		) 
		{
			++gCon.scoreElement.innerHTML;
			this.reset();
		};

		//Boarders of canvas
		if(this.player.x - this.canvas.width > 0)
		{
			this.player.x = -20;
			 // As the canvas is draw from the top left 0 would be the boarder on that axis
		}
		else if(this.player.y - this.canvas.height > 0)
		{
			this.player.y = -20;
		}
		else if(this.player.x - this.canvas.width + 1000 < 0)
		{
			this.player.x = this.canvas.width;
		}
		else if(this.player.y - this.canvas.height + 680 < 0)
		{
			this.player.y = this.canvas.height;
		}
		
	},

	//drawBackground
	drawBackground: function()
	{

		this.context.drawImage(this.background, 0, 0);
	},
	
	//drawPlayer
	drawPlayer: function()
	{
		this.context.drawImage(this.playerImg, this.player.x, this.player.y);
	},
	drawEnemy: function()
	{
		this.context.drawImage(this.enemyImg, this.enemy.x, this.enemy.y);
	},
	//togglePaused
	togglePaused: function()
	{
		var now = +new Date();
		this.paused = !this.paused;
		if(this.paused)
		{
			this.pauseStartTime = now;
		}
		else
		{
			this.lastAnimationFrameTime += (now - this.pauseStartTime);
		}
	},

	//Save Score
	saveScore: function(now)
	{
		if(this.serverAvailable)
      {
      	this.socket.emit('DogeMileHighClub', 
      	{
      		score: gCon.scoreElement.innerHTML
      	});
      }
	},

	loadingAnimationLoaded: function()
	{
		if(!this.gameStarted)
		{
			this.fadeInElements(this.AnimatedGifElement, this.loadingTitleElement);
		}
	},

	dimControls: function ()
	{
		FINAL_OPACITY = 0.5;
		gCon.instructionsElement.style.opacity = FINAL_OPACITY;
	},
	//Reveals Canvas
	revealCanvas: function ()
	{
		this.fadeInElements(this.canvas);
	},
	//Reveal All score,fps and other options
	revealTopChrome: function()
	{
		this.fadeInElements(this.fpsElement, this.scoreElement);
	},

	revealTopChromeDimmed: function ()
	{
		var DIM = 0.25;

		this.scoreElement.style.display = 'block';
		this.fpsElement.style.display = 'block';

		setTimeout(function ()
		{
			gCon.scoreElement.style.opacity = DIM;
			gCon.fpsElement.style.opacity = DIM;
		}, this.SHORT_DELAY);
	},

	revealBottomChrome: function()
	{
		this.fadeInElements(this.instructionsElement, this.copyRightElement);
	},
	//reveal Game
	revealGame: function()
	{
		var DIM_CONTROLS_DELAY = 4000;

		this.revealTopChromeDimmed();
		this.revealCanvas();
		this.revealBottomChrome();

		setTimeout(function ()
		{
			gCon.dimControls();
			gCon.revealTopChrome();
		}, DIM_CONTROLS_DELAY);
	},
	revealToast: function(text, duration){
		var DEFAULT_TOAST_DISPLAY_DURATION = 1000;
		duration = duration || DEFAULT_TOAST_DISPLAY_DURATION;

		this.startToastTransition(text, duration);

		setTimeout(function (e)
		{
			
			gCon.hideToast();
			
		}, duration);
	},
	//reveal Initial Toast
	revealInitialToast: function()
	{
		var INITIAL_TOAST_DELAY = 1000,
		INITIAL_TOAST_DURATION = 1500;

		setTimeout(function()
		{
			gCon.revealToast('Get Doge and endure', INITIAL_TOAST_DELAY);
		}, INITIAL_TOAST_DURATION);
	},
	//TOAST
	hideToast: function()
	{
		var TOAST_TRANSITION_DURATION = 450;
		this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
	},

	//This SHOULD set the text of the toast 
	//and fade it in over the supplied duration
	startToastTransition: function(text, duration){
		this.toastElement.innerHTML = text;
		gCon.fadeInElements(this.toastElement);
	},
	//FADES
	fadeInElements: function ()
	{
		var args = arguments;
		for(var i=0; i < args.length; ++i)
		{
			console.log(args[i]);
			args[i].style.display = 'block';
		}

		setTimeout(function () {
			for(var i=0; i < args.length; ++i)
			{
				args[i].style.opacity = gCon.OPAQUE;
			}
		}, this.SHORT_DELAY);
	},

	fadeOutElements: function ()
	{
		var args = arguments,
			fadeDuration = args[args.length-1];
		for(var i=0; i < args.length-1; ++i)
		{
			console.log(args[i]);
			args[i].style.opacity = gCon.TRANSPARENT;
		}

		setTimeout(function () {
		for(var i=0; i < args.length-1; ++i)
		{
			args[i].style.display = 'none';
		}
		}, fadeDuration);
	},
	reset: function()
	{
		//enemy somewhere on the screen randomly
		this.enemy.x = 32 + (Math.random() * (this.canvas.width - 64));
		this.enemy.y = 32 + (Math.random() * (this.canvas.height - 64));
	}
	
};

var keysDown = {};

addEventListener("keydown", function (e) {
	keysDown[e.keyCode] = true;

	if(e.keyCode === 80)
	{
		gCon.saveScore();
		gCon.togglePaused();
	}
}, false);

addEventListener("keyup", function (e) {
	delete keysDown[e.keyCode];
}, false);

window.addEventListener('blur', function (e){
	gCon.windowHasFocus = false;
	if(!gCon.paused)
	{
		gCon.togglePaused();
	}
});

window.addEventListener('focus', function (e){
	var originalFont = gCon.toastElement.style.fontSize,
		DIGIT_DISPLAY_DURATION = 1000; //milliseconds
	gCon.windowHasFocus = true;
	gCon.countdownInProgress = true;
	if(gCon.paused)
	{
		gCon.toastElement.style.font = '142px Impact';
		if(gCon.windowHasFocus && gCon.countdownInProgress)
		{
			gCon.revealToast('3', DIGIT_DISPLAY_DURATION);
		}
		setTimeout(function (e)
		{
			if(gCon.windowHasFocus && gCon.countdownInProgress)
				gCon.revealToast('2', DIGIT_DISPLAY_DURATION);
			
			setTimeout(function (e)
			{
				if(gCon.windowHasFocus && gCon.countdownInProgress)
					gCon.revealToast('1', DIGIT_DISPLAY_DURATION);
			
				setTimeout(function (e)
				{
					if(gCon.windowHasFocus && gCon.countdownInProgress)
					{
						gCon.togglePaused();
					}
					if(gCon.windowHasFocus && gCon.countdownInProgress)
					{
						gCon.toastElement.style.fontSize = originalFont;
					}
					gCon.countdownInProgress = false;
				}, DIGIT_DISPLAY_DURATION);
			}, DIGIT_DISPLAY_DURATION);
		}, DIGIT_DISPLAY_DURATION); 
	}
});

var gCon = new gCon();
gCon.initializeImages();